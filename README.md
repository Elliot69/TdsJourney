# The TD's Journey

This project aims at recreating the typical hectic daily life of a Technical Director in an Animation/VFX studio.
Dependencies:
- python 2.7
- module pyside
- module enum

Run the following commands after cloning to start the journey:

```bash
$ cd TdsJourney
$ python -m application
```