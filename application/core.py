# coding: utf-8

import random
import json
from enum import Enum
import math
import io


# --------------------------------- #
#             Profile               #
# --------------------------------- #


class Characteristic:
    """
    Represents a generic scored characteristic.

    A Characteristic's score can only be incremented.
    """

    def __init__(self, name, value=0, min_value=0, max_value=100):
        self.name = name
        self.value = value
        self.min_value = min_value
        self.max_value = max_value
    
    def add(self, value):
        """ Update the Characteristic's value within its definition interval """
        self.value += value

        # Clamp to interval bounds
        self.value = max(self.min_value, min(self.max_value, self.value))


class Profile:
    """
    Describes the TD's skills and abilities to endure life.
    """

    def __init__(self):
        # Make characteristics accessible by name
        self.attributes = []
        self.skills = []
        self.attributes_indices = {}
        self.skills_indices = {}
        
        # Initialize attributes
        self._add_attribute('social', 50)
        self._add_attribute('professional', 50)
        self._add_attribute('emotional', 50)

        # Initialize skills
        self._add_skill('rd', 0)
        self._add_skill('communication', 0)
        self._add_skill('debug', 0)
        self._add_skill('software', 0)
    
    def update_profile_from_dicts(self, attribute_points, skill_points):
        # Update attribute values
        for attribute_point in attribute_points.items():
            index = self.attributes_indices[attribute_point[0]]
            self.attributes[index].add(attribute_point[1])
        
        # Update skill values
        for skill_point in skill_points.items():
            index = self.skills_indices[skill_point[0]]
            self.skills[index].add(skill_point[1])
    
    def get_attributes_as_dict(self):
        attribute_dict = {}

        for attribute in self.attributes:
            attribute_dict[attribute.name] = attribute.value
        
        return attribute_dict
    
    def get_skills_as_dict(self):
        skill_dict = {}

        for skill in self.skills:
            skill_dict[skill.name] = skill.value
        
        return skill_dict
    
    def _add_attribute(self, name, value):
        self.attributes_indices[name] = len(self.attributes)
        self.attributes.append(Characteristic(name, value))
    
    def _add_skill(self, name, value):
        self.skills_indices[name] = len(self.skills)
        self.skills.append(Characteristic(name, value))


# --------------------------------- #
#            Scheduling             #
# --------------------------------- #


class PeriodType(Enum):
    NIGHT = 0
    MORNING = 1
    MIDDAY = 2
    AFTERNOON = 3
    EVENING = 4


class Period:
    """
    Represents a period of a day.
    """

    def __init__(self, type, start_time):
        self.type = type
        self.start_time = start_time


class Time:
    
    def __init__(self, float_time):
        self.hours = str(int(math.floor(float_time)))
        self.minutes = str(int((float_time - math.floor(float_time)) * 60))
    
    def __repr__(self):
        return '<{class_name}({hours}:{minutes})>'.format(
            class_name=self.__class__.__name__,
            hours=self.hours,
            minutes=self.minutes
        )


class Scheduler:
    """
    Manages time progression at a week's scale, and within each day.
    """
    DAY_DURATION = 24.0
    
    def __init__(self):
        self.days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday' ]
        self.current_day = 0
        self.time_of_day = 0.0

        self.periods = [
            Period(PeriodType.NIGHT,  0.0),
            Period(PeriodType.MORNING, 9.0),
            Period(PeriodType.MIDDAY, 13.0),
            Period(PeriodType.AFTERNOON, 14.0),
            Period(PeriodType.EVENING, 19.0)
        ]
    
    def progress_in_time(self, time):
        self.time_of_day += time

        if self.time_of_day > self.DAY_DURATION:
            self.time_of_day -= self.DAY_DURATION
            self.current_day += 1
    
    def get_time(self):
        return Time(self.time_of_day)
    
    def get_day(self):
        return self.days[self.current_day]
    
    def get_period_type(self):
        for period in self.periods[::-1]:
            if self.time_of_day >= period.start_time:
                return period.type


# --------------------------------- #
#          Tasks and Events         #
# --------------------------------- #


class Event:
    """ Class defining all Events' basic behavior"""
    def __init__(self, dataDict):
        self.identifier = ''
        self.title = ''
        self.phase = []
        self.description = ''
        self.requiredTime = 0
        self.answers = []
        self.data = dataDict
        self.triggerTask = False

        self._set_attributes()
        self._set_answers()

    def _set_attributes(self):
        """ Set all Task's attributes by parsing info contained into the given dataDict given
        at initialization of each new Task object."""
        self.identifier = self.data.get('identifier', '')
        self.phase = self.data.get('phase', '')
        self.title = self.data.get('title', '')
        self.description = self.data.get('description', '')
        self.requiredTime = self.data.get('requiredTime')

    def _set_answers(self):
        """ Generates an instance of Answer class for each answer found in the Task's data dict"""
        answersList = self.data.get('answers', '')
        for answerData in answersList:
            newAnswer = Answer(answerData) # We create an answer object by passing it its info
            self.answers.append(newAnswer) # We add this newly created answer object to the answer list of the current Task.


class Task:
    """ Class defining all Tasks' basic behavior"""
    # TODO: Maybe the Task class should inherit from the Event class. See how possible can that be once the Task-generating events handled.

    def __init__(self, dataDict):
        self.identifier = ''
        self.title = ''
        self.requiredTime = 0.0
        self.data = dataDict
        self.steps = []
        self.currentStep = 0

        self._set_attributes()
        self._set_steps()
    
    def get_current_step(self):
        return self.steps[self.currentStep]

    def get_description(self):
        """ Gets description from the current step"""
        for step in self.steps:
            stepNumber = step.data.get('number', '')
            if not stepNumber == self.currentStep:
                continue
            description = step.data.get('description', '')
            return description
    
    def increment_step(self):
        """ Increments the Task's step """
        self.currentStep += 1

    def _set_attributes(self):
        """ Set all Task's attributes by parsing info contained into the given dataDict given
        at initialization of each new Task object."""
        self.identifier = self.data.get('identifier', '')
        self.title = self.data.get('title', '')
        self.requiredTime = self.data.get('requiredTime')

    def _set_steps(self):
        """ Generates an instance of Step class for each step found in the Task's data dict"""
        steps = self.data.get('steps')
        for stepData in steps:
            newStep = Step(stepData)
            self.steps.append(newStep)

class Step:
    def __init__(self, dataDict):
        self.number = 0
        self.description = ''
        self.data = dataDict
        self.answers = []

        self._set_answers()

    def _set_answers(self):
        """ Generates an instance of Answer class for each answer found in the Task's data dict"""
        answersList = self.data.get('answers', '')
        for answerData in answersList:
            newAnswer = Answer(answerData) # We create an answer object by passing it its info
            self.answers.append(newAnswer) # We add this newly created answer object to the answer list of the current Task.

class Answer:
    def __init__(self, answerData):
        self.description = ''
        self.skills = {}
        self.attributes = {}
        self.data = answerData

        self._set_attributes()

    def _set_attributes(self):
        """ Sets attributes of the answer depending on the data given as argument on instance initialization"""
        if not self.data:
            return
        self.description = self.data.get('description')
        self.skills = self.data.get('skills')
        self.attributes = self.data.get('attributes')


# --------------------------------- #
#          Simulator Model          #
# --------------------------------- #


class TDSimulator:
    """
    Represents the application's model.
    """

    def __init__(self):
        self.profile = Profile()
        self.scheduler = Scheduler()
        self.existingTasks = [] # List of instances of Task
        self.currentTask = None # Instance of Task
        self.event = None # Instance of Event
        self.current_type = 'task'

        self.generate_tasks()
        self.set_current_task(self.existingTasks[0].identifier)

    def generate_tasks(self):
        """ Generates a list of tasks, by randomly picking tasks info in the data file"""
        with io.open('application/data/tasks.json', 'r', encoding='utf-8') as dataFile:
            tasks = json.load(dataFile)

        for i in range(5):
            # tasks = dataDict.get('tasks', '')
            taskInfo = random.choice(tasks)
            task = Task(taskInfo)
            self.existingTasks.append(task)

    def set_current_task(self, identifier):
        """ Identifies the current task from the given task identifier

        :param identifier: identifier of the current task
        :type identifier: str
        """
        for task in self.existingTasks:
            if not task.identifier == identifier:
                continue
            self.currentTask = task

    def progress_current_task(self, answer):
        """ Increments the step number of the current task

        :param answer: identifier of the current task
        :type answer: str
        """

        self.scheduler.progress_in_time(float(self.currentTask.requiredTime))
        self.answer_task(answer)
        self.currentTask.increment_step()
        
        if self.currentTask.currentStep == len(self.currentTask.steps):
            self.complete_task(self.currentTask)

    def answer_task(self, answerDescription):
        """ Generating the impact of the event's answer chosen by the user by getting the answer's skill and attributes
        points and passing it to the profile handler so update the profile attributes consequently.

        :param answerDescription: description of the answer chosen by the user
        :type answerDescription: str
        """
        taskAnswers = self.currentTask.get_current_step().answers
        for answer in taskAnswers:
            if not answer.description == answerDescription:
                continue

            skillPoints = answer.skills
            attributesPoints = answer.attributes

            self.profile.update_profile_from_dicts(attribute_points=attributesPoints,
                                                   skill_points=skillPoints)
    def complete_task(self, task):
        """ Completes a task by removing it from the list of existing tasks, and triggering an event

        :param task: Completed task
        :type task: instance of Task
        """
        self.existingTasks.remove(task)
        self.currentTask = None
        self.trigger_event()

    def answer_event(self, answerDescription):
        """ Generating the impact of the event's answer chosen by the user by getting the answer's skill and attributes
        points and passing it to the profile handler so update the profile attributes consequently.

        :param answerDescription: description of the answer chosen by the user.
        :type answerDescription: str
        """
        eventAnswers = self.event.answers
        for answer in eventAnswers:
            if not answer.description == answerDescription:
                continue

            skillPoints = answer.skills
            attributesPoints = answer.attributes

            self.profile.update_profile_from_dicts(attribute_points=attributesPoints,
                                                   skill_points=skillPoints)
        
        self.current_type = 'task'
        self.currentTask = None

    def trigger_event(self):
        """ When the task is completed, triggers a new event, and sets it as the current event."""
        with io.open('application/data/events.json', 'r', encoding='utf-8') as dataFile:
            events = json.load(dataFile)

        eventInfo = random.choice(events)
        event = Event(eventInfo)
        self.event = event
        self.current_type = 'event'
    
    def get_simulator_as_dict(self):
        values = {}

        values["clock"] = [ 
            self.scheduler.get_day(),
            self.scheduler.get_time().hours,
            self.scheduler.get_time().minutes
        ]

        task_ids = []
        task_titles = []
        
        for task in self.existingTasks:
            task_ids.append(task.identifier)
            task_titles.append(task.title)

        values["taskId"] = task_ids
        values["tasks"] = task_titles
        
        if self.current_type == 'task':
            description = "" if self.currentTask is None else self.currentTask.get_description()
        else:
            description = "" if self.event is None else self.event.description
        
        values["text"] = description

        answers = []

        if self.current_type == 'task' and self.currentTask:
            for answer in self.currentTask.get_current_step().answers:
                answers.append(answer.description)
        elif self.event:
            for answer in self.event.answers:
                answers.append(answer.description)

        values["answer"] = answers

        attributes = self.profile.get_attributes_as_dict()
        skills = self.profile.get_skills_as_dict()
        
        values["points"] = {
            'professionalLife': attributes['professional'],
            'socialLife': attributes['social'],
            'burnOut': attributes['emotional'],
            'dev': skills['rd'],
            'communication': skills['communication'],
            'debug': skills['debug'],
            'sofware': skills['software']
        }

        values["type"] = self.current_type

        return values


def test():
    # Profile test
    print('\n=================== Profile tests ===================')
    test_profile = Profile()

    attribute_points = { 'professional': 25, 'emotional': -15 }
    skill_points = { 'rd': 10, 'communication': 5, 'software': 110 }
    test_profile.update_profile_from_dicts(attribute_points, skill_points)

    expected_attributes = { 'social': 50, 'professional': 75, 'emotional': 35 }
    expected_skills = { 'rd': 10, 'communication': 5, 'debug': 0, 'sofware': 100 }
    
    print('Attributes : %s' % test_profile.get_attributes_as_dict())
    assert test_profile.get_attributes_as_dict()
    print('Skills : %s' % test_profile.get_skills_as_dict())
    assert test_profile.get_skills_as_dict()

    # Scheduler test
    print('\n================== Scheduler tests ==================')
    test_scheduler = Scheduler()

    test_scheduler.progress_in_time(38.5)

    print('Day : %s' % test_scheduler.get_day())
    assert test_scheduler.get_day() == 'Tuesday'
    print('Period : %s' % test_scheduler.get_period_type())
    assert test_scheduler.get_period_type() == PeriodType.AFTERNOON
    print('Time : %s' % test_scheduler.get_time())
    assert test_scheduler.get_time().hours == "14"
    assert test_scheduler.get_time().minutes == "30"

    # Random tests
    print('\n================== Random tests ==================')
    print('Nothing interesting here for now')

    # TD Simulator test
    # Scheduler test
    print('\n================ Simulator tests =================')
    simulator = TDSimulator()
    print(simulator.get_simulator_as_dict())


if __name__ == '__main__':
    test()