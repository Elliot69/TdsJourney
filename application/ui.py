# -*- coding: utf-8 -*-
import sys

try:
    from PySide2 import QtWidgets
except ImportError:
    from PySide import QtGui
    # Redirect QtWidgets (is this still legal ?)
    QtWidgets = QtGui


class MainWindow(QtWidgets.QWidget) :

    def __init__ (self, application) :

        # start up datas---------------------------

        self.application = application

        self.inputDico = self.application.get_values()


        # create widgets--------------------------


        QtWidgets.QWidget.__init__(self)

        self.mainLayout = QtWidgets.QVBoxLayout()

        self.mainPanels = self.allPanels()
        #self.mainPanels.setFixedWidth(1000)
        
        self.mainLayout.addWidget(self.mainPanels)
        
        
        self.setLayout(self.mainLayout)

    def allPanels(self) : 

        self.allPanelsLayout = QtWidgets.QVBoxLayout()
        self.allPanelsWidget = QtWidgets.QWidget()

        self.widget1 = self.upperPanels()
        self.widget2 = self.calendarPanel()

        self.allPanelsLayout.addWidget(self.widget1)
        self.allPanelsLayout.addWidget(self.widget2)

        self.allPanelsWidget.setLayout(self.allPanelsLayout)

        return self.allPanelsWidget
   
    def upperPanels(self) : 

        upperPanelsLayout = QtWidgets.QHBoxLayout()
        upperPanelsWidget = QtWidgets.QWidget()
        upperPanelsLayout.addWidget(self.progressPanel())
        upperPanelsLayout.addWidget(self.dialogPanel())
        upperPanelsLayout.addWidget(self.taskPanel())
        upperPanelsWidget.setLayout(upperPanelsLayout)

        return upperPanelsWidget

    def progressPanel(self) :

        progressPanelLayout = QtWidgets.QVBoxLayout()

        widgetProgressBars = QtWidgets.QWidget()
        widgetProgressBars.setFixedWidth(150)

        widgetText1 = QtWidgets.QLabel("Skills")
        
        self.widgetSkillBar1 = ProgressBar(self.inputDico["points"]["dev"],"Dev/R&D","indigo") 
        self.widgetSkillBar2 = ProgressBar(self.inputDico["points"]["communication"],"Communication","pink")
        self.widgetSkillBar3 = ProgressBar(self.inputDico["points"]["debug"],"Debug","yellow")
        self.widgetSkillBar4 = ProgressBar(self.inputDico["points"]["sofware"],"Software","orange")

        widgetText2 = QtWidgets.QLabel("Score")
        self.widgetScoreBar1 = ProgressBar(self.inputDico["points"]["professionalLife"],"Vie Professionelle","cyan")
        self.widgetScoreBar2 = ProgressBar(self.inputDico["points"]["socialLife"],"Vie Sociale","purple")
        self.widgetScoreBar3 = ProgressBar(self.inputDico["points"]["burnOut"],"Emotionnel","red")
        
        
        progressPanelLayout.addWidget(widgetText1)
        progressPanelLayout.addWidget(self.widgetSkillBar1)
        progressPanelLayout.addWidget(self.widgetSkillBar2)
        progressPanelLayout.addWidget(self.widgetSkillBar3)
        progressPanelLayout.addWidget(self.widgetSkillBar4)

        progressPanelLayout.addWidget(widgetText2)
        progressPanelLayout.addWidget(self.widgetScoreBar1)
        progressPanelLayout.addWidget(self.widgetScoreBar2)
        progressPanelLayout.addWidget(self.widgetScoreBar3)
        
        widgetProgressBars.setLayout(progressPanelLayout)

        return widgetProgressBars

    def dialogPanel(self) :

        

        self.dialogPanelLayout = QtWidgets.QVBoxLayout()

        dialogPanelWidget = QtWidgets.QWidget()
        dialogPanelWidget.setFixedWidth(500)

        self.widgetText = QtWidgets.QLabel(self.inputDico["text"])
       
        self.widgetText.setWordWrap(True)

        self.dialogPanelLayout.addWidget(self.widgetText)
        
        self.dialogButtonGroup = QtWidgets.QButtonGroup()
        self.dialogButtonGroup.exclusive()

        i = -1

        if self.inputDico["text"] != "":

            for answer in self.inputDico["answer"]:

                i = i+1

                self.widgetAnswer = QtWidgets.QPushButton(answer)
                self.widgetAnswer.setFlat(False)
                self.widgetAnswer.setCheckable(True)
            
                self.dialogPanelLayout.addWidget(self.widgetAnswer)

                self.dialogButtonGroup.addButton(self.widgetAnswer)
                self.dialogButtonGroup.setId(self.widgetAnswer, i)


        widgetConfirm = QtWidgets.QPushButton("Repondre")
        widgetConfirm.clicked.connect(self.confirmButton)
        self.dialogPanelLayout.addWidget(widgetConfirm)

        dialogPanelWidget.setLayout(self.dialogPanelLayout)

        return dialogPanelWidget

    def taskPanel(self) :
        
        self.taskPanelLayout = QtWidgets.QVBoxLayout()
        self.taskPanelWidget = QtWidgets.QWidget()

        if self.inputDico["tasks"] != "":

            self.taskButtonGroup = QtWidgets.QButtonGroup()
            self.taskButtonGroup.exclusive()


            i = -1

            for task in self.inputDico["tasks"]: 

                i = i+1

                self.widgetTask = QtWidgets.QPushButton(task)
            
                self.widgetTask.setFlat(False)
                self.widgetTask.setCheckable(True)

                self.widgetTask.clicked.connect(self.taskButton)
            
                self.taskPanelLayout.addWidget(self.widgetTask)

                self.taskButtonGroup.addButton(self.widgetTask)
                self.taskButtonGroup.setId(self.widgetTask, i)


        self.taskPanelWidget.setLayout(self.taskPanelLayout)

        return self.taskPanelWidget

    def calendarPanel(self) :

        calendarPanelLayout = QtWidgets.QHBoxLayout()
        calendarPanelWidget = QtWidgets.QWidget()

        self.widgetClock = QtWidgets.QLabel("{} / {}:{}".format(self.inputDico["clock"][0],self.inputDico["clock"][1],self.inputDico["clock"][2]))
       
        calendarPanelLayout.addWidget(self.widgetClock)

        calendarPanelWidget.setLayout(calendarPanelLayout)

        return calendarPanelWidget

    #NOT USED def choicePanel(self,answers,buttonType):

        self.buttonGroup = QtWidgets.QButtonGroup()
        self.buttonGroup.exclusive()

        self.choicePanelLayout = QtWidgets.QVBoxLayout()
        self.choicePanelWidget = QtWidgets.QWidget()

        i = -1

        for answer in answers:

            i = i+1

            if buttonType == "QCheckBox" :

                self.widgetAnswer = QtWidgets.QCheckBox(answer)

            if buttonType == "QPushButton" :

                self.widgetAnswer = QtWidgets.QPushButton(answer)
                self.widgetAnswer.setFlat(False)
                self.widgetAnswer.setCheckable(True)
           
            self.choicePanelLayout.addWidget(self.widgetAnswer)

            self.buttonGroup.addButton(self.widgetAnswer)

            self.buttonGroup.setId(self.widgetAnswer, i)

            self.choicePanelWidget.setLayout(self.choicePanelLayout)

        return self.choicePanelWidget

    def confirmButton(self) :


        # Return checked choice 
        
       
        dialogId = self.dialogButtonGroup.checkedId()        
        dialogToReturn = self.inputDico["answer"][dialogId]

        if dialogId != -1 :

       
            if self.inputDico["type"] == "task" :

                self.application.progress_current_task(dialogToReturn)


            else :  

                self.application.answer_event(dialogToReturn)
        
        
        
        
        #self.application.select_task(taskIdToReturn)
        
                    
            # Kill all panel

            self.mainPanels.close()

            # Update dico

            self.inputDico = self.application.get_values()

            # Rebuilt all panel

            self.mainPanels = self.allPanels()
            self.mainLayout.addWidget(self.mainPanels)



        else :

            # Kill all panel

            self.mainPanels.close()

            # Update dico

            self.inputDico = self.application.get_values()

            # Rebuilt all panel

            self.mainPanels = self.allPanels()
            self.mainLayout.addWidget(self.mainPanels)

    def taskButton(self) :
        
        taskId = self.taskButtonGroup.checkedId()
        taskIdToReturn = self.inputDico["taskId"][taskId]
        
        self.application.select_task(taskIdToReturn)
       
 
        # Kill all panel

        self.mainPanels.close()

        # Update dico

        self.inputDico = self.application.get_values()

        # Rebuilt all panel

        self.mainPanels = self.allPanels()
        self.mainLayout.addWidget(self.mainPanels)



        
def ProgressBar(value,texte,color) :


    bar = QtWidgets.QProgressBar() 
    bar.text()
    bar.setTextVisible(True)
    bar.setMinimum(0)
    bar.setMaximum(100)
    bar.setValue(value)
    bar.setFormat(texte)
    bar.setStyleSheet("QProgressBar::chunk {} background-color: {};{} ".format("{",color,"}"))
    bar.setFormat(texte)

    return bar
















if __name__ == "__main__":

    game = QtWidgets.QApplication(sys.argv)
    mainWindow = MainWindow("test")
    mainWindow.show()
    game.exec_()
    


        