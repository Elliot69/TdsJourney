import sys
import logging
from application.ui import MainWindow
from application.core import TDSimulator

try:
    from PySide2 import QtWidgets
except ImportError:
    from PySide import QtGui
    # Redirect QtWidgets (is this legal ?)
    QtWidgets = QtGui


class TDSimulatorApp:
    """
    Represents the simulator controller.
    """

    def __init__(self, args):
        self.pyside_app = QtWidgets.QApplication(args)
        self.simulator = TDSimulator()  # Initialize current day's event scheduler
        self.main_window = MainWindow(self)

    def run(self):
        self.main_window.show()
        return self.pyside_app.exec_()

    def select_task(self, identifier):
        self.simulator.set_current_task(identifier)

    def progress_current_task(self, answer):
        self.simulator.progress_current_task(answer)

    def answer_event(self, answerDescription):
        self.simulator.answer_event(answerDescription)
    
    def get_values(self):
        return self.simulator.get_simulator_as_dict()


if __name__ == '__main__':
    application = TDSimulatorApp(sys.argv)
    sys.exit(application.run())


